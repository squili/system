{pkgs, ...}: {
  programs = {
    zsh = {
      enable = true;
      enableAutosuggestions = true;
      enableVteIntegration = true;
      autocd = true;
      history.save = 100;
      initExtra = builtins.readFile ./zshrc;
      shellAliases = {
        ls = "${pkgs.eza}/bin/eza";
      };
      syntaxHighlighting.enable = true;
    };

    starship = {
      enable = true;
      settings = {
        format = "$directory$character";
        right_format = "$all";
        add_newline = false;
      };
    };

    carapace.enable = true;
    zoxide.enable = true;
  };
}
