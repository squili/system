{pkgs, ...}: {
  programs.git = {
    enable = true;
    lfs.enable = true;
    userEmail = "mia@mia.jetzt";
    userName = "mia";
    extraConfig = {
      init.defaultBranch = "main";
      push.autoSetupRemote = true;
    };
  };

  programs.gh.enable = true; # needed by carapace

  home.packages = [pkgs.gitoxide];
}
