{pkgs, ...}: {
  programs.vscode = {
    enable = true;
    enableExtensionUpdateCheck = false;
    enableUpdateCheck = false;
    mutableExtensionsDir = false;
    package = pkgs.vscodium;
    extensions = with pkgs.vscode-extensions;
      [
        elixir-lsp.vscode-elixir-ls
        golang.go
        jnoortheen.nix-ide
        kamadorueda.alejandra
        ms-python.python
        ms-vscode.cpptools
        ms-vscode.hexeditor
        piousdeer.adwaita-theme
        rust-lang.rust-analyzer
        serayuzgur.crates
        skellock.just
        sumneko.lua
        tamasfe.even-better-toml
        tiehuis.zig
        timonwong.shellcheck
      ]
      ++ builtins.map (ref: pkgs.vscode-utils.buildVscodeMarketplaceExtension {mktplcRef = ref;}) [
        {
          publisher = "BytecodeAlliance";
          name = "wit-idl";
          version = "0.3.1";
          sha256 = "sha256-AW+TaVGdXqUxNP0swk6xBNTiFTEn5D2CbF+9LYcTRnU=";
        }
        {
          publisher = "grain-lang";
          name = "vscode-grain";
          version = "0.18.2";
          sha256 = "sha256-F4QnVJTWCX9ma8TqqZPOdNm7OZSYE7iPr34Y7FvMJng=";
        }
        {
          publisher = "dtsvet";
          name = "vscode-wasm";
          version = "1.4.1";
          sha256 = "sha256-zs7E3pxf4P8kb3J+5zLoAO2dvTeepuCuBJi5s354k0I=";
        }
        {
          publisher = "biomejs";
          name = "biome";
          version = "2.1.1";
          sha256 = "sha256-ME5HxuorkFoPzLzOFOQJyDaI6qSvkEI9rviXbrw+PjM=";
        }
        {
          publisher = "Vue";
          name = "volar";
          version = "1.8.27";
          sha256 = "sha256-6FktlAJmOD3dQNn2TV83ROw41NXZ/MgquB0RFQqwwW0=";
        }
      ];
    keybindings = [
      # intellij-style quickfix (sorry) (im not)
      {
        key = "alt+enter";
        command = "editor.action.quickFix";
        when = "editorHasCodeActionsProvider && textInputFocus && !editorReadonly";
      }
      {
        key = "ctrl+.";
        command = "-editor.action.quickFix";
        when = "editorHasCodeActionsProvider && textInputFocus && !editorReadonly";
      }
      # why isn't this here by default
      {
        key = "ctrl+shift+r";
        command = "workbench.action.tasks.runTask";
      }
    ];
    userSettings = {
      "C_Cpp.default.compilerPath" = "/etc/profiles/per-user/user/bin/g++";

      "editor.bracketPairColorization.enabled" = false;
      "editor.formatOnSave" = true;

      "nix.serverPath" = pkgs.nixd + /bin/nixd;
      "nix.enableLanguageServer" = true;

      "terminal.integrated.cursorStyle" = "line";

      "workbench.colorTheme" = "Adwaita Dark & default syntax highlighting";
    };
  };

  home.packages = with pkgs; [alejandra biome nixd elixir-ls];
}
