{
  config,
  pkgs,
  sops-nix,
  ...
}: {
  imports = [
    ./git.nix
    ./shell
    ./vscode.nix
  ];

  home = {
    packages = with pkgs; [
      # applications
      audacity
      element-desktop
      gimp
      gnome.dconf-editor
      jetbrains.idea-community
      kdeconnect
      libsForQt5.kate
      mpv
      rhythmbox
      signal-desktop
      vesktop
      zoom-us

      # command line
      btop
      dig
      eza
      ffmpeg
      file
      fx
      jq
      just
      ripgrep
      shellcheck
      statix
      tokei
      wasm-tools
      watchexec
      whois
      wit-bindgen
      xh
      yt-dlp

      # language
      dart-sass
      elixir
      gcc
      nasm
      python3
      ruby
      rustup

      # resource
      man-pages
    ];

    language = {
      base = "en_US.UTF8";
      time = "en_GB.UTF8";
    };
  };

  programs = {
    home-manager.enable = true;
    firefox.enable = true;
    direnv.enable = true;
  };

  nixpkgs.config = import ./nixpkgs-config.nix;
  xdg.configFile."nixpkgs/config.nix".source = ./nixpkgs-config.nix;

  home.stateVersion = "23.11";
}
