# recovery system, installed to /etc/nixos as a backup
{pkgs, ...}: {
  imports = [./hardware-configuration.nix];

  services = {
    fwupd.enable = true;
    xserver = {
      enable = true;
      displayManager.sddm.enable = true;
      desktopManager.plasma5.enable = true;
      libinput.enable = true;
    };
  };

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  networking.hostName = "framework";
  networking.networkmanager.enable = true;
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  users.users.user = {
    isNormalUser = true;
    home = "/home";
    extraGroups = ["wheel"]; # Enable ‘sudo’ for the user.
  };
  environment.systemPackages = with pkgs; [
    curl
    git
  ];
  nix.settings.experimental-features = ["nix-command" "flakes"];
  system.stateVersion = "23.11";
}
