{pkgs, ...}: {
  imports = [
    ./hardware-configuration.nix
  ];

  # boot
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking = {
    hostName = "framework";
    firewall = {
      enable = true;
      allowedTCPPortRanges = [
        {
          from = 1714;
          to = 1764;
        }
      ]; # kde connect
      allowedUDPPortRanges = [
        {
          from = 1714;
          to = 1764;
        }
      ]; # kde connect
    };
    wireless = {
      enable = true;
      environmentFile = "/run/secrets/wpa_env";
      networks = {
        "WelcomeToTheCorps".psk = "@PWD_HOME@";
      };
    };
  };

  # locale
  time.timeZone = "America/Los_Angeles";
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.supportedLocales = ["en_GB.UTF-8/UTF-8" "en_US.UTF-8/UTF-8"];

  services = {
    xserver = {
      enable = true;
      displayManager.sddm.enable = true;
      desktopManager.plasma5.enable = true;
      libinput.enable = true;
    };
    fprintd.enable = true;
    printing.enable = true;
    fwupd.enable = true;
  };

  # audio
  sound.enable = true;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
  };

  # user
  users.users.user = {
    isNormalUser = true;
    extraGroups = ["wheel"];
    home = "/home";
    shell = pkgs.zsh;
  };

  environment.systemPackages = with pkgs; [curl git];
  environment.pathsToLink = ["/share/zsh"];
  programs = {
    noisetorch.enable = true;
    npm.enable = true;
    steam.enable = true;
    zsh.enable = true;
  };

  sops = {
    defaultSopsFile = ../secrets.yaml;
    age.keyFile = "/home/.config/sops/age/keys.txt";
    secrets.wpa_env = {};
  };

  # nix
  nix.settings.experimental-features = ["nix-command" "flakes"];
  nixpkgs.config.allowUnfree = true;

  system.stateVersion = "23.11"; # note: do not fucking touch
}
