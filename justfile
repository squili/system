nixpkgs-channel := "release-23.11"

clean:
    sudo nix-collect-garbage -d

switch:
    set -e
    git add .
    statix check
    nixos-rebuild build --flake .# # check if it successfully builds
    sudo nixos-rebuild switch --flake .#

recovery:
    sudo cp nixos/recovery.nix /etc/nixos/configuration.nix
    sudo cp nixos/hardware-configuration.nix /etc/nixos/hardware-configuration.nix

sops:
    EDITOR='codium -w' nix run nixpkgs/{{nixpkgs-channel}}#sops -- secrets.yaml
